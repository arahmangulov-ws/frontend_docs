/**
 * Created by iamzero76 on 14.10.16.
 */

'use strict';

// Импорт плагинов
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    imageResize = require('gulp-image-resize'),
    fileRenamer = require('gulp-rename'),
    plumber = require('gulp-plumber');

// Пути к исходному коду проекта, директории сборки
var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        css: 'build/style/',
        img: 'build/img/src',
        img_min: 'build/img/preview',
        fonts: 'build/fonts/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.php', //Синтаксис src/*.php говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
        style: 'src/style/main.scss',
        img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        img_min: 'build/img/src/*.*', // Берем из папки build, т.к. нам нужны уже сжатые версии
        fonts: 'src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/**/*.php',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

// Сборка html
gulp.task('html:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

// Сборка Javascript
gulp.task('js:build', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write("./")) //Пропишем карты
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

// Сборка CSS
gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(plumber({
            hundleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(reload({stream: true}));
});

// Сборка и создание превьюшек, сжимаем (да-да, до 2-х шакалов)
gulp.task('image:build', function () {
    return gulp.src(path.src.img) //Выберем наши картинки
        .pipe(fileRenamer({ // Переименуем сжатые по размеру файлы
            suffix: '_b' // И добавим суффикс _b (big)
        }))
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

//
gulp.task('image_min:build', ['image:build'], function () {
    gulp.src(path.src.img_min) //Берем уже сжатые images
        .pipe(fileRenamer(function (opt) { // Сначала уберем суффикс для большой картинки
            opt.basename = opt.basename.replace(/_b$/, '');
            return opt
        }))
        .pipe(fileRenamer({
            suffix: '_m' // И добавим новый суффикс _m (min)
        }))
        .pipe(imageResize({
            width: 100, // Уменьшим их до заданной ширины
            crop: false, // Не будем использовать "нарезку"
            upscale: false
        }))
        .pipe(gulp.dest(path.build.img_min)) //Кладем в папку build/preview
        .pipe(reload({stream: true}));
});

// Копируем шрифты
gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('browserSync', function() {
    browserSync.init({
        proxy: "localhost:63342/project-polina/build/"
    });
});

// Кон*ечные мероприятия по объединению задач
gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'fonts:build',
    'image_min:build'
]);

// Работем, пока изменяются файлы (строятся небоскребы)
gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image_min:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});


// Можно убрать за собой
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// Финал-очка
gulp.task('default', ['build', 'watch', 'browserSync']);

